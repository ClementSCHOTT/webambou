laissez mainNavLinks =  document . querySelectorAll ( " nav ul li a " );
let mainSections =  document . querySelectorAll ( " section principale " );

laissez lastId;
laisser cur = [];

// Cela devrait probablement être étranglé.
// Surtout parce qu'il se déclenche lors d'un défilement régulier.
// https://lodash.com/docs/4.17.10#throttle
// Tu pourrais faire comme ...
// window.addEventListener ("scroll", () => {
//     _.throttle (doThatStuff, 100);
// });
// Seulement, ne pas le faire ici pour garder ce stylo sans dépendance.

fenêtre . addEventListener ( " scroll " , event  => {
  let fromTop =  window . scrollY ;

  mainNavLinks . forEach ( link  => {
    let section =  document . querySelector ( link . hash );

    si (
      section . offsetTop  <= fromTop &&
      section . section offsetTop  +  . offsetHeight > fromTop 
    ) {
      lien . classList . add ( " current " );
    } else {
      lien . classList . remove ( " current " );
    }
  });
});